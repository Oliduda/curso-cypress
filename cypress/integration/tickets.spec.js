//const { it } = require("mocha");

//import { it } from 'mocha';
import Tickets from '../support/pages/Ticketbox';
describe("Tickets", () => {
    

    it("preenche todos os campos de entrada de texto", () => {

        Tickets.acessarTelaInicial();
        Tickets.cadastrarBilhete();
        Tickets.validarCadastroComSucesso();
    });

    it("Selecioar dois Tickets", () => {
        cy.get("#ticket-quantity").select("2");
    });

    it("Selecionar o ticket 'vip'", () => {
        cy.get("#vip").check();
    });

    it("Selecionar 'social media' checkbox", () => {
        cy.get("#social-media").check();
    });

    it("Selecionar 'amigo', e 'publico', em seguida desmarcar 'amigo'", () => {
        cy.get("#friend").check();
        cy.get("#publication").check();
        cy.get("#friend").uncheck();
    });

    it("has 'TICKETBOX' header's heading", () => {
        cy.get("header h1").should("contain", "TICKETBOX");
    });

    it("Alerta sobre email Invalido", () => {
        cy.get("#email")
            .as("email")
            .type("mariaeduarda-gmail.com");

        cy.get("#email.invalid").should("exist");

        cy.get("@email")
            .clear()
            .type("mariaeduarda@gmail.com");

        cy.get("#email.invalid").should("not.exist");

    });

    it("Preecnher e redefinir o formulário", () => {
        const primeironome = "Eduarda";
        const ultimonome = "Oliveira";
        const fullName = `${primeironome} ${ultimonome}`;

        cy.get("#first-name").type(primeironome);
        cy.get("#last-name").type(ultimonome);
        cy.get("#email").type("maria@gmail.com");
        cy.get("#ticket-quantity").select("2");
        cy.get("#vip").check();
        cy.get("#friend").check();
        cy.get("#requests").type("IPA beer");

        cy.get(".agreement p").should(
            "contain",
            `I, ${fullName}, wish to buy 2 VIP tickets.`
        );

        cy.get("#agree").click();
        cy.get("#signature").type(fullName);

        cy.get("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled");

        cy.get("button[type='reset']").click();

        cy.get("@submitButton").should("be.disabled");
    });

    it("Preencher os campos obrigatórios usando comandos de suporte", () => {
        const customer = {
            primeiroNome: "Maria",
            ultimoNome: "Soares",
            email: "maria@gmail.com"
        };

        cy.fillMandatoryFields(customer);

        cy.get("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled");

        cy.get("#agree").uncheck();

        cy.get("@submitButton").should("be.disabled");

    });


});