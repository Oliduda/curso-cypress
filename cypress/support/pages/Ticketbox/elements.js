export const ELEMENTS = {
    primeiroName: '#first-name',
    ultimoNome: '#last-name',
    email: '#email',
    quantidade: '#ticket-quantity',
    tipo: '#vip',
    evento: '#friend',
    requerimentos: '#requests',
    acordo: '#agree',
    assinatura: '#signature',
    botaoConfirmar: '.active',
    botaoReset: '.reset'
  }
