const el = require('./elements').ELEMENTS;

class Ticketbox {
    acessarTelaInicial() {
        cy.visit('https://bit.ly/2XSuwCW');
    }

    cadastrarBilhete() {
        cy.get(el.primeiroName).type("Eduarda");
        cy.get(el.ultimoNome).type("Oliveira");
        cy.get(el.email).type("maria@gmail.com");
        cy.get(el.quantidade).select("2");
        cy.get(el.tipo).check();
        cy.get(el.evento).check();
        cy.get(el.requerimentos).type("IPA beer");
        cy.get(el.acordo).click();
        cy.get(el.assinatura).type("Maria Eduarda");
        cy.get(el.botaoConfirmar).click();
    }
    validarCadastroComSucesso(){
        cy.get(".success > p").contains("Ticket(s) successfully ordered.");
    }

}
export default new Ticketbox();